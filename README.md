# poject2

[![CI Status](https://img.shields.io/travis/eduardo parada pardo/poject2.svg?style=flat)](https://travis-ci.org/eduardo parada pardo/poject2)
[![Version](https://img.shields.io/cocoapods/v/poject2.svg?style=flat)](https://cocoapods.org/pods/poject2)
[![License](https://img.shields.io/cocoapods/l/poject2.svg?style=flat)](https://cocoapods.org/pods/poject2)
[![Platform](https://img.shields.io/cocoapods/p/poject2.svg?style=flat)](https://cocoapods.org/pods/poject2)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

poject2 is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'poject2'
```

## Author

eduardo parada pardo, udelunar@gmail.com

## License

poject2 is available under the MIT license. See the LICENSE file for more info.
